import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import ProductApi from '@/api/product'
import OrderModule from './order';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

const store = () => new Vuex.Store({
  plugins: [vuexLocal.plugin],
  state: {
    locales: ['en', 'ru'],
    locale: 'ru',
    products: [],
    user: {},
    user_role: null
  },
  mutations: {
    SET_LANG (state, locale) {
      if (state.locales.includes(locale)) {
        state.locale = locale
      }
    },
    SET_PRODUCTS (state, products = []) {
      state.products = products
    },
    SET_USER (state, user) {
      state.user = user;
      if (user) {
        state.user_role = user.role;
      }
    }
  },
  actions: {
    async LOAD_CURRENT_USER ({commit}) {
      const user = window.localStorage.getItem('user');
      console.log('user', JSON.parse(user));
      commit('SET_USER', JSON.parse(user));
    },
    async LOAD_PRODUCTS ({commit}) {
      const products = await ProductApi.getProducts();
      commit('SET_PRODUCTS', products);
      return products;
    }
  },
  modules: {
    order: OrderModule
  }
});

export default store
