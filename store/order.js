import PostApi from '@/api/post';
import ProductApi from '@/api/product'
import OrderApi from '@/api/order'
import { validate } from 'vee-validate';
const {parsePhoneNumberFromString} = require('libphonenumber-js');

import productTypes from '@/constants/productTypes';
import productCounts from '@/constants/productCounts';

export default {
  namespaced: true,
  state () {
    return {
      selected_product: null,
      products: [],
      order_lines: {
        product_id: null,
        quantity: 1,
        type: null,
        stickers: []
      },
      total_price: 0,
      user_name: '',
      last_name: '',
      city: '',
      address: '...',
      phone_number: '',
      email: null,
      comment: '',
      promo_code: null,
      cities: [],
      post_offices: [],
      selected_post_office: null,
      is_agreement_signed: true,
      is_order_ready_to_checkout: null,
      selected_promo: null,
      order_id: null,
      // This field to show promo price
      full_price: null
    }
  },
  mutations: {
    RESET_ORDER_STATE (state) {
      console.log('reset order state');
      state.selected_product = null ;
      state.products = [];
      state.full_price = null;
      state.order_id = null;
      state.selected_promo = null;
      state.is_agreement_signed = true;
      state.is_order_ready_to_checkout = undefined;
      state.order_lines = {
        product_id: null,
        quantity: 1,
        type: null,
        stickers: []
      };
      state.total_price = 0;
      state.comment = '';
    },
    SET_SELECTED_PRODUCT (state, product) {
      state.selected_product = product;
    },
    INIT_SINGLE_STICKER_PACK (state, product) {
      const stickers = [];
      const files = [];
      for (let i = 0; i < productCounts.STICKERS_IN_PACK; i++) {
        files.push({
          file: null,
          img_src: null,
          is_focused: false,
          is_valid: true
        })
      }
      stickers.push({
        files: files
      });

      state.order_lines = {
        product_id: product.id,
        quantity: 1,
        type: product.type,
        stickers: stickers
      };
      state.total_price = product.discounted_price ? product.discounted_price : product.total_price;
    },
    INIT_MULTI_STICKER_PACK (state, product) {
      const stickers = [];
      const files = [];

      for (let i = 0; i < productCounts.STICKERS_IN_PACK; i++) {
        files.push({
          file: null,
          img_src: null,
          is_valid: true,
          is_focused: false
        })
      }
      stickers.push({
        files: files
      });
      stickers.push({
        files: [...files]
      });
      state.order_lines = {
        product_id: product.id,
        quantity: 2,
        type: product.type,
        stickers: stickers
      };
      const totalPrice = product.discounted_price ? product.discounted_price : product.total_price;
      state.total_price = totalPrice;
    },
    INIT_CONTOUR_STICKER_PACK (state, product) {
      state.order_lines = {
        product_id: product.id,
        quantity: 1,
        type: product.type,
        stickers: [{
          files: [{
            file: null,
            img_src: null,
            is_valid: true,
            is_focused: true
          }]
        }]
      };
      const totalPrice = product.discounted_price ? product.discounted_price : product.total_price;

      state.total_price = totalPrice;

    },
    INIT_CUP (state, product) {
      state.order_lines = {
        product_id: product.id,
        quantity: 1,
        type: product.type,
        cup_text: '',
        stickers: [{
          files: [{
            file: null,
            img_src: null,
            is_valid: true,
            is_focused: true
          }]

        }]
      };
      const totalPrice = product.discounted_price ? product.discounted_price : product.total_price;

      state.total_price = totalPrice;

    },
    INIT_STATIC_STICKERS (state, { product, staticStickersProducts }) {

      state.static_stickers_products = staticStickersProducts;

      const thumbnails = staticStickersProducts.map(sticker => {
        return {
          id: sticker.id,
          type: sticker.type,
          url: sticker.thumbnail_url
        }
      });

      const previews = staticStickersProducts.map(sticker => {
        return {
          id: sticker.id,
          type: sticker.type,
          url: sticker.preview_url
        }
      });


      const templates = staticStickersProducts.map(sticker => {
        return {
          id: sticker.id,
          type: sticker.type,
          url: sticker.template_url
        }
      });
      state.order_lines = {
        product_id: product.id,
        quantity: 1,
        type: product.type,
        stickers: [{
          files: [{
            thumbnails,
            previews,
            templates,
            selected_stickerpack_id: thumbnails[0].id,
            is_valid: true
          }]
        }]
      };

      const totalPrice = product.discounted_price ? product.discounted_price : product.total_price;

      state.total_price = totalPrice;
    },
    ADD_STICKER_PACK (state) {
      const quantity = state.order_lines.quantity;
      const productType = state.order_lines.type;
      const stickers = state.order_lines.stickers;

      const updatedQuantity = quantity + 1;
      if (productType === productTypes.SINGLE_STICKER_PACK) {
        let multiStickerPackProduct = null;
        state.products.forEach(product => {
          if (product.type === productTypes.MULTI_STICKER_PACK) {
            multiStickerPackProduct = product;
          }
        });


        const files = [];
        stickers[0].files.forEach(file => {
          files.push({
            ...file
          })
        });
        stickers.push({
          files: files
        });

        state.order_lines.product_id = multiStickerPackProduct.id;
        state.order_lines.quantity = updatedQuantity;
        state.order_lines.type = multiStickerPackProduct.type;
        state.selected_product = multiStickerPackProduct;
        state.total_price = multiStickerPackProduct.discounted_price || multiStickerPackProduct.total_price;
      } else {
        const files = [];
        if (productType === productTypes.MULTI_STICKER_PACK) {
          stickers[0].files.forEach(file => {
            files.push({
              ...file
            })
          });
        }
        if (productType === productTypes.CONTOUR_STICKER) {
          stickers[0].files.forEach(file => {
            files.push({
              ...file
            })
          });
        }

        if (productType === productTypes.CUP) {
          stickers[0].files.forEach(file => {
            files.push({
              ...file
            })
          });
        }

        if (productType === productTypes.STATIC_PACK_ANIMALS) {
          // handle
          stickers[0].files.forEach(file => {
            files.push({
              ...file,
              selected_stickerpack_id: file.thumbnails[0].id,
            })
          });
        }

        if (productType === productTypes.STATIC_PACK_SCIENTISTS) {
          // handle
          stickers[0].files.forEach(file => {
            files.push({
              ...file,
              selected_stickerpack_id: file.thumbnails[0].id
            })
          });
        }


        state.order_lines.stickers.push({
          files: files
        });

        state.order_lines.quantity = updatedQuantity;

        const basicPrice = state.selected_product.discounted_price ? state.selected_product.discounted_price : state.selected_product.total_price;
        const additionalItemsPrice = state.selected_product.price_per_additional_item * (quantity - 1);
        state.total_price = basicPrice + additionalItemsPrice;

      }


    },
    REMOVE_STICKER_PACK (state, $index) {
      const quantity = state.order_lines.quantity;
      const stickers = state.order_lines.stickers;
      const updatedQuantity = quantity - 1;
      const productType = state.order_lines.type;
      if (updatedQuantity === 1 && productType === productTypes.MULTI_STICKER_PACK) {
        let singleStickerPackProduct = null;
        state.products.forEach(product => {
          if (product.type === productTypes.SINGLE_STICKER_PACK) {
            singleStickerPackProduct = product;
          }
        });

        state.order_lines.type = singleStickerPackProduct.type;
        state.order_lines.product_id = singleStickerPackProduct.id;
        state.order_lines.quantity = updatedQuantity;
        state.selected_product = {...singleStickerPackProduct};
        const totalPrice = singleStickerPackProduct.discounted_price || singleStickerPackProduct.total_price;
        state.total_price = totalPrice;
      } else {
        state.order_lines.quantity = updatedQuantity;
        state.total_price = state.total_price - state.selected_product.price_per_additional_item;

      }
      stickers.splice($index, 1);
    },
    SET_FOCUS (state, sticker) {
      const stickerPackIndex = sticker.sticker_pack_index;
      const fileIndex = sticker.file_index;
      state.order_lines.stickers.forEach(sticker => {
        sticker.files.forEach(file => {
          file.is_focused = false;
        })
      });
      const existingSticker = state.order_lines.stickers[stickerPackIndex].files[fileIndex];
      state.order_lines.stickers[stickerPackIndex].files.splice(fileIndex, 1, {
        ...existingSticker,
        is_focused: true,
        is_valid: true
      });
      console.log('sticker', stickerPackIndex);
    },
    FILL_STICKER (state, sticker) {
      const stickerPackIndex = sticker.sticker_pack_index;
      const fileIndex = sticker.file_index;

      // save it to S3 with fine uploader.
      // save url

      state.order_lines.stickers[stickerPackIndex].files.splice(fileIndex, 1, {
        img_src: sticker.img_src,
        file: null,
        is_valid: true,
        is_loading: true,
        is_focused: false
      });
      console.log('fill sticker', sticker.sticker_pack_index, sticker.file_index);
    },
    FILL_STICKER_PATH (state, sticker) {
      console.log('fill sticker path', sticker.sticker_src, sticker.file.name);
      const stickerPackIndex = sticker.sticker_pack_index;
      const fileIndex = sticker.file_index;

      // save it to S3 with fine uploader.
      // save url

      state.order_lines.stickers[stickerPackIndex].files.splice(fileIndex, 1, {
        img_src: sticker.img_src,
        file: {
          name: sticker.file.name,
          path: sticker.sticker_src
        },
        is_valid: true,
        is_loading: false,
        is_focused: false
      });
    },

    FILL_STATIC_STICKERPACK (state, {stickerpack_index, selected_slide_index}) {
      state.order_lines.stickers[stickerpack_index].files.splice(0, 1, {
        ...state.order_lines.stickers[stickerpack_index].files[0],
        selected_stickerpack_id: state.order_lines.stickers[stickerpack_index].files[0].thumbnails[selected_slide_index].id,
        is_valid: true,
      });
    },

    SET_PRODUCTS (state, products) {
      state.products = products;
    },
    TOGGLE_AGREEMENT (state, value) {
      console.log('is agreement signed value', value);
      state.is_agreement_signed = value;
    },
    ADD_USER_NAME (state, userName) {
      console.log('user name', userName);
      state.user_name = userName
    },
    ADD_LAST_NAME (state, last_name) {
      console.log('user name', last_name);
      state.last_name = last_name
    },
    ADD_CITY (state, city) {
      console.log('city', city);
      state.city = city
    },
    ADD_ADDRESS (state, address) {
      console.log('address', address);

      state.address = address
    },
    ADD_PHONE_NUMBER (state, phoneNumber) {
      console.log('phoneNumber', phoneNumber);

      state.phone_number = phoneNumber
    },
    ADD_EMAIL (state, email) {
      console.log('email', email);

      state.email = email
    },
    ADD_COMMENT (state, comment) {
      console.log('comment', comment);
      state.comment = comment
    },
    ADD_PROMO_CODE (state, promoCode) {
      console.log('promo code', promoCode);
      state.promoCode = promoCode
    },
    ADD_POST_OFFICES (state, postOffices = []) {
      state.post_offices = postOffices
    },
    SELECT_POST_OFFICE (state, postOffice) {
      console.log('post office', postOffice);
      state.selected_post_office = postOffice
    },
    ADD_CITIES (state, cities) {
      console.log('city', cities);
      state.cities = cities
    },
    IS_ORDER_READY_TO_CHECKOUT (state, flag) {
      state.is_order_ready_to_checkout = flag;
    },
    SET_ORDER_ID (state, order_id) {
      state.order_id = order_id;
    },
    APPLY_PROMO_CODE (state, promoCode) {
      state.selected_promo = promoCode;
    },
    CALCULATE_TOTAL_PRICE (state) {
      const product = state.selected_product;
      const quantity = state.order_lines.quantity;
      const promoCode = state.selected_promo;
      let totalPrice = 0;
      totalPrice = product.discounted_price || product.total_price;
      if (quantity > 1) {
        let additionalItems = quantity - 1;
        if (product.type === productTypes.MULTI_STICKER_PACK) {
          additionalItems = quantity - 2;
        }

        totalPrice = totalPrice + (additionalItems * product.price_per_additional_item);
      }
      if (promoCode && promoCode.discount_percentage) {
        const discountPrice = Math.ceil(totalPrice * (promoCode.discount_percentage / 100));
        state.full_price = totalPrice;
        totalPrice = Math.ceil(totalPrice - discountPrice);
      } else {
        state.full_price = null;
      }
      state.total_price = totalPrice;
    }
  },
  getters: {},
  actions: {
    async LOAD_CITIES ({commit}) {
      const citiesFromLocalStorage = window.localStorage.getItem('cities');
      if (citiesFromLocalStorage) {
        commit('ADD_CITIES', JSON.parse(citiesFromLocalStorage));
        return citiesFromLocalStorage;
      } else {
        const cities = await PostApi.getCities();
        window.localStorage.setItem('cities', JSON.stringify(cities));
        commit('ADD_CITIES', cities);
        return cities;
      }
    },
    async FIND_PRODUCT ({commit}, productPermalink) {
      const defaultType = productTypes.SINGLE_STICKER_PACK;
      const products = await ProductApi.getProducts();
      let selectedProduct = null;
      if (productPermalink) {
        products.forEach(product => {
          if (product.permalink === productPermalink) {
            selectedProduct = {...product};
          }
        })
      }
      if (!selectedProduct) {
        products.forEach(product => {
          if (product.type === defaultType) {
            selectedProduct = {...product};
          }
        });
      }
      commit('SET_SELECTED_PRODUCT', selectedProduct);
      commit('SET_PRODUCTS', products);
      return selectedProduct;
    },
    async INIT_STICKERS ({commit, dispatch}, productPermalink) {
      console.log('init stickers called');
      const product = await dispatch('FIND_PRODUCT', productPermalink);
      const {SINGLE_STICKER_PACK, MULTI_STICKER_PACK, CONTOUR_STICKER, CUP, STATIC_PACK_ANIMALS, STATIC_PACK_SCIENTISTS} = productTypes;
      switch (product.type) {
        case SINGLE_STICKER_PACK: {
          commit('INIT_SINGLE_STICKER_PACK', product);
          break;
        }
        case MULTI_STICKER_PACK: {
          commit('INIT_MULTI_STICKER_PACK', product);
          break;
        }
        case CONTOUR_STICKER: {
          commit('INIT_CONTOUR_STICKER_PACK', product);
          break;
        }
        case CUP: {
          commit('INIT_CUP', product);
          break;
        }
        case STATIC_PACK_ANIMALS: {
          const staticStickers = await ProductApi.getStaticStickersByType(product.type);
          commit('INIT_STATIC_STICKERS', {
            product,
            staticStickersProducts: staticStickers
          });
          break;
        }
        default: {
          commit('INIT_SINGLE_STICKER_PACK', product);
        }
        commit('CALCULATE_TOTAL_PRICE');
      }
    },
    async ADD_MORE_STICKERS ({commit, state}) {
      commit('ADD_MORE_STICKERS');
    },
    REMOVE_STICKER_PACK ({commit}, index) {
      commit('REMOVE_STICKER_PACK', index);
    },
    async CHECK_PROMO ({commit}, promoCode) {
       const response = await OrderApi.checkPromoCode(promoCode);
      console.log('response', response);
      commit('ADD_PROMO_CODE', promoCode);

      if (response.has_promo) {
        commit('APPLY_PROMO_CODE', response.promo_code);
      } else {
        commit('APPLY_PROMO_CODE', null);
      }
      commit('CALCULATE_TOTAL_PRICE');
      return response;
    },
    async VALIDATE_ORDER ({commit, state}) {
      let isStickersValid = null;

        state.order_lines.stickers.forEach((stickerPack, stickerIndex) => {
          isStickersValid = stickerPack.files.some((sticker, fileIndex) => {
            if (sticker.file === null) {
              state.order_lines.stickers[stickerIndex].files.splice(fileIndex, 1, {
                file: null,
                is_valid: false,
                is_focused: false,
                img_src: null
              });
              return false;
            }
            return true
          });
        });

        const userNameValidation = await validate(state.user_name, 'required');
        const lastNameValidation = await validate(state.last_name, 'required');
        const cityValidation = await validate(state.city, 'required');
        const phoneNumberValidation = await validate(state.phone_number, 'required|phone');
        const postOfficeValidation = await validate(state.selected_post_office, 'required');
        const agreementValidation = state.is_agreement_signed;
        const emailValidation = await validate(state.email, 'required|email');

        const isValid = isStickersValid && emailValidation.valid && userNameValidation.valid && lastNameValidation.valid && cityValidation.valid && phoneNumberValidation.valid && postOfficeValidation.valid && agreementValidation;
        commit('IS_ORDER_READY_TO_CHECKOUT', isValid);
        return isValid;
    },
    async CHECKOUT_ORDER ({commit, state, dispatch}) {
        const isValid = await dispatch('VALIDATE_ORDER');
        if (isValid) {
          const phoneNumber = parsePhoneNumberFromString(state.phone_number);

          const deliveryInformation = {
            user_name: `${state.user_name} ${state.last_name}`,
            city: state.city,
            address: 'MOCK_FOR_NOW',
            phone_number: phoneNumber.number,
            email: state.email,
            comment: state.comment,
            post_office: state.selected_post_office
          };
          const orderLines = {
            product_id: state.order_lines.product_id,
            type: state.order_lines.type,
            quantity: state.order_lines.quantity
          };

          const promoCode = state.selected_promo ? state.selected_promo.promo_code : null;
          const order = await OrderApi.createOrder({
            order_lines: orderLines,
            delivery_information: deliveryInformation,
            files: state.order_lines.stickers,
            promo_code: promoCode
          });

          commit('SET_ORDER_ID', order.id);

          setTimeout(() => {
            document.getElementById('liqpay-form').submit();
          }, 0);

        }
    },
    async FILL_STICKER ({commit, state, dispatch}, sticker) {
      commit('FILL_STICKER', sticker);
      const response = await OrderApi.saveSticker(sticker.file);
      console.log('response', response);
      commit('FILL_STICKER_PATH', {
        ...sticker,
        sticker_src: response.sticker_src
      });
    }
  }
}
