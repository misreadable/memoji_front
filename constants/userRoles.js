export default {
  CLIENT: 'client',
  PRINTING: 'printing',
  DELIVERY: 'delivery',
  MANAGER: 'manager',
  ADMIN: 'admin',
  SUPERADMIN: 'superadmin'
}
