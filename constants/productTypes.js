export default {
  SINGLE_STICKER_PACK: 'single_sticker_pack',
  MULTI_STICKER_PACK: 'multi_sticker_pack',
  CONTOUR_STICKER: 'contour_sticker',
  FORM_STICKER_SQUARE: 'form_sticker_square',
  FORM_STICKER_ROUND: 'form_sticker_round',
  SHIRT: 'shirt',
  CUP: 'cup',
  STATIC_PACK_ANIMALS: 'static_pack_animals',
  STATIC_PACK_SCIENTISTS: 'static_pack_scientists'
};
