import Vue from 'vue'

export const isInViewPort = {
  isLinear: true,
  inserted: (el, binding, vnode) => {
    let func = () => {
      let rect = el.getBoundingClientRect()
      let inView = (
        rect.top >= 0 &&
        rect.bottom <=(window.innerHeight || document.documentElement.clientHeight));

      if (inView) {
        el.classList.add(binding.value)
        window.removeEventListener('scroll', func)
      }
    }
    window.addEventListener('scroll', func);
    func();
  }
}

Vue.directive('is-in-view-port', isInViewPort);