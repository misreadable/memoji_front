import {get} from 'lodash'

export default function ({
                           route,
                           $axios,
                           redirect
                         }) {
  $axios.defaults.timeout = 30000
  //  $axios.setToken(localStorage.getItem('token'), 'Bearer')

  $axios.onRequest(() => {
    // $axios.setToken(localStorage.getItem('token'), 'Bearer')
  })

  $axios.onError((error) => {
    const status = get(error, 'response.status')
  })
}
