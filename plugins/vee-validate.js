import Vue from 'vue';
import {ValidationProvider, ValidationObserver, extend} from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';

const {parsePhoneNumberFromString} = require('libphonenumber-js');

extend('required', {
  ...required,
  message: 'Это поле нужно заполнить :('
});

extend('min', {
  params: ['length'],
  validate: (value, {length}) => value !== null && value.length <= length,
  message: 'Это поле нужно заполнить :('
});

extend('post_declaration_number', {
  validate: (value) => value !== null && value.length <= 14,
  message: 'Валидный ТТН - 14 цифр'
});

extend('delivery_cost', {
  validate: (value) => parseFloat(value),
  message: 'Нужно заполнить цену доставки'
});


extend('email', {
  ...email,
  message: 'Должен быть валидный email'
});

extend('phone', {
  validate: (value) => {

    const phoneNumber = parsePhoneNumberFromString(value);
    if (phoneNumber && value && value.length === 17) {
      return phoneNumber.isValid();
    }
  },
  message: 'Должен быть валидный телефон'
});

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
