import Vue           from 'vue'
import 'vue-awesome/icons/trash'
import 'vue-awesome/icons/times-circle'


import Icon from 'vue-awesome/components/Icon'

Vue.component('v-icon', Icon);
