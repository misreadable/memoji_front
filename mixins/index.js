import userRoles from '@/constants/userRoles';


export const isAdmin = {
  methods: {
    isAdmin (userRole) {
      console.log('user_idle', userRole);
      return userRole === userRoles.ADMIN || userRole === userRoles.SUPERADMIN;
    }
  }
};
