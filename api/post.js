import http from '@/services/http'

export default {
  async getPostOffices () {
    return http.get('/postOffices');
  },
  async getCities () {
    return http.get('/cities');
  },
  async getOfficesByCityId () {
    return http.get('/officesByCityId');
  },
}
