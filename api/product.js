import http from '@/services/http'

export default {
  async getProducts () {
    return http.get('/products');
  },
  async getStaticStickersByType (type) {
    return http.get(`/getStaticStickersByType/${type}`);
  },
}
