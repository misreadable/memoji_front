import http from '@/services/http'

export default {
  async saveSticker (sticker) {
    const form = new FormData();
    form.append('sticker', sticker);
    return http.post('/saveSticker', form).catch((err) => {
      console.error('create order', err);
    });
  },
  async createOrder ({order_lines, files = [], delivery_information, promo_code}) {
    const stickers = {};
    files.forEach((stickerPack, stickerIndex) => {
      stickerPack.files.forEach((sticker, index) => {
        // only for static stickers
        if (sticker.selected_stickerpack_id) {
          stickers[`sticker_${stickerIndex + 1}_${index + 1}`] = {
            id: sticker.selected_stickerpack_id
          };
          return;
        }
        if (sticker.file && !sticker.file.name) {
          stickers[`sticker_${stickerIndex + 1}_${index + 1}`] = {
            name: `sticker_${stickerIndex+1}_${index+1}`,
            path: sticker.img_src
          };
          return;
        }

        stickers[`sticker_${stickerIndex + 1}_${index + 1}`] = sticker.file;
      })
    });

    return http.post('/createOrder', {
      order_lines: [order_lines],
      delivery_information,
      promo_code,
      files: stickers,
    }).catch((err) => {
      console.error('create order', err);
    });
  },

  async applyPromoCode (orderId, promoCode) {
    return http.post('/applyPromoCode', {
      order_id: orderId,
      promo_code: promoCode
    });
  },

  async checkPromoCode (promoCode) {
    return http.post('/checkPromoCode', {
      promo_code: promoCode
    });
  },

  async getOrderForClient (orderId) {
    return http.get(`/getOrderForClient/${orderId}`);
  },

  async getPromoForFriend () {
    return http.get('/getPromoForFriend');
  }
}
