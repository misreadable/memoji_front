import http from '@/services/http'

export default {
  async login (phoneNumber) {
    return http.post('admin/login', {
      phone_number: phoneNumber
    });
  },

  async verifyCode (phoneNumber, verificationCode) {
    return http.post('admin/verifyCode', {
      phone_number: phoneNumber,
      verification_code: verificationCode
    }).catch(err => {
      return err;
    });
  },

  async getPromoCodes () {
    return http.get('/admin/promoCodes');
  },

  async createPromoCode ({promo_code, discount_percentage, allowed_count_of_use}) {
    return http.post('/admin/promoCodes', {
      promo_code,
      discount_percentage,
      allowed_count_of_use
    });
  },

  async deactivatePromoCode (discountId) {
    return http.post('/admin/deactivatePromoCode', {
      discount_id: discountId
    });
  },

  async getOrders ({order_status, skip, limit}) {
    return http.post('/admin/orders', {
      order_status,
      skip,
      limit
    })
  },

  async getOrdersByPhoneNumber (phone_number) {
    return http.post('/admin/getOrdersByPhoneNumber', {
      phone_number
    });
  },

  async sendOrderToPrint (orderId) {
    return http.post('/admin/sendOrderToPrint', {
      order_id: orderId
    });
  },

  async sendOrderToReadyForDelivery (orderId) {
    return http.post('/admin/sendOrderToReadyForDelivery', {
      order_id: orderId
    });
  },

  async sendOrderToDelivery ({order_id, post_invoice_number, delivery_price}) {
    return http.post('/admin/sendOrderToDelivery', {
      order_id,
      post_invoice_number,
      delivery_price
    });
  },

  async sendOrderToDelivered (orderId) {
    return http.post('/admin/sendOrderToDelivered', {
      order_id: orderId
    });
  },
}
