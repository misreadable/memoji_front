import {ENV} from './env';

let API_URL = 'http://localhost:3100/api';

if (ENV === 'development') {
  API_URL = 'http://localhost:3100/api';
}

if (ENV === 'staging') {
  API_URL = 'https://api.memoji.shop/api';
}

if (ENV === 'production') {
  API_URL = 'https://api.memoji.shop/api';
}

export default API_URL;
