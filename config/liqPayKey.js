import {ENV} from './env';

let keys = {};
if (ENV === 'development' || ENV === 'staging') {
  keys = {
    PUBLIC_KEY: 'sandbox_i93105135708',
    PRIVATE_KEY: 'sandbox_Um6YSNZUSxi8Gs94RZSC93ptRxs6aiEr8gio6z0J'
  }
}

if (ENV === 'production') {
  keys = {
    PUBLIC_KEY: 'i65415781345',
    PRIVATE_KEY: 'b1Ux1vtOexBs2aHaQRVdD2hrENp2z8lSW9k3bvVZ'
  }
}

export default {
  ...keys
}
