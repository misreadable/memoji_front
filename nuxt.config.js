module.exports = {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content:
          'width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      },
      {
        hid: 'og:title',
        name: 'og:title',
        property: 'og:title',
        content: 'Memoji Shop'
      },
      {
        hid: 'og:description',
        name: 'og:description',
        property: 'og:description',
        content: 'We are about stickers printing here'
      },
      {
        hid: 'og:image',
        name: 'og:image',
        property: 'og:image',
        content: 'https://memoji.shop/img/facebook-card.jpg'
      },
      {
        hid: 'og:url',
        name: 'og:url',
        property: 'og:url',
        content: 'https://memoji.shop/'
      },
      {
        hid: 'og:site_name',
        name: 'og:site_name',
        property: 'og:site_name',
        content: 'Memoji Shop'
      },
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        property: 'twitter:card',
        content: 'summary'
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        property: 'twitter:title',
        content: 'Memoji Shop'
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        property: 'twitter:description',
        content: 'We are about stickers printing here'
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        property: 'twitter:image',
        content: 'https://memoji.shop/img/twitter-card.jpg'
      }
    ],
    link: [{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {color: '#fff'},
  /*
   ** Global CSS
   */
  css: [
    'swiper/dist/css/swiper.css'
    // '~/assets/style/app.styl',
  ],
  styleResources: {
    stylys: [
      'assets/style/main.css'
    ]
  },
  router: {
    middleware: 'i18n'
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/plugins/axios',
    // '@/plugins/global-mixins',
    '~/plugins/i18n.js',
    '~/plugins/vee-validate',
    '~/plugins/vue-notification',
    '~/plugins/vue-icons',
    '~/plugins/vue-mask',
    { src: '~/plugins/vuex-persist', ssr: false },
    { src: '~/plugins/swiper.js', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
    ['@nuxtjs/google-analytics', {
      id: 'UA-149080207-1'
    },
      // opions only for debug
      {
        debug: {
          enabled: true,
          sendHitTask: true
        }
      }],
    ['@nuxtjs/google-tag-manager', {
      id: 'GTM-NMMKFCC'
    }]
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources'
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
    }
  }
}
